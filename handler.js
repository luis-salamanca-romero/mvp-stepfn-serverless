'use strict';

const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();
module.exports.executeStepFunction =  (event,context,callback) => {
  console.log('executeStepFunction');

  const number = event.queryStringParameters.number;
  console.log(number);

  callStepFunction(number).then(result => {
    let message = 'Ejecutando Step Function';
    if(!result){
      message = 'No se Ejecuto la Step Function';
    }
    else{
      message = result;
    }
    const response =  {
      statusCode: 200,
      body: JSON.stringify({
          message: message
        })
    };
    callback(null, response);
  });
};

module.exports.calculateRandomNumber = (event,context,callback) => {
  console.log('calculateRandomNumber ha sido llamada');

  let result = parseInt(event.number) + 1;
  console.log(result);
  callback(null,{result});
};

module.exports.secondCalculateNumber = (event,context,callback)=>{
  console.log("llamando a secondCalculateNumber");
  
  let result = parseInt(event.result) + 2;
  console.log(result);

  callback(null,{ result });
};

module.exports.thirdCalculateNumber = (event,context,callback)=> {
  console.log("llamando a thirdCalculateNumber");
  
  let result = parseInt(event.result) + 3;
  console.log(result);

  callback(null,{ result });
};

module.exports.fourCalculateNumber = (event,context,callback)=> {
  console.log("llamando a fourCalculateNumber");
  
  let result = parseInt(event.result) + 4;
  console.log(result);

  
  callback(null, result);
  
};


function callStepFunction(number){
  console.log('callStepFunction');

  const stateMachineName = 'testingStateMachine';
  
  return stepfunctions.listStateMachines({}).promise().then(listStateMachines => {
      console.log('Buscando step  function', listStateMachines);

      for (var i = 0; i < listStateMachines.stateMachines.length; i++) {
        const item = listStateMachines.stateMachines[i];

        if (item.name.indexOf(stateMachineName) >= 0) {
          console.log('se encontro la step function', item);

          var params = {
            stateMachineArn: item.stateMachineArn,
            input: JSON.stringify({ number: number })
          };

          console.log('Start execution');
          return stepfunctions.startExecution(params).promise().then(async data => {
            console.log('==> data', data)
            await new Promise(r => setTimeout(r, 2000));
            return stepfunctions.describeExecution({ executionArn: data.executionArn}).promise();
          })
          .then(result => {
            console.log(result);
            return  result.output;
          });
        }
      }
  })
  .catch(error => {
    return false;
  });
}